package UI;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import PageObjects.HomePageBaycity;
import PageObjects.HomePageSherman;
import PageObjects.MoviesPageBaycity;
import PageObjects.ShowtimesPageBaycity;
import PageObjects.ShowtimesPageSherman;
import resources.base;

public class VerifyShowtimesSherman extends base {
	
	public static Logger log=LogManager.getLogger(base.class.getName());
	
	@BeforeTest
	public void initialize() throws IOException
	{
		driver = InitializeDriver();
		driver.manage().window().maximize();
	}
	
	@Test
	public void VerifyShowtimesSherman() throws InterruptedException {
		driver.get(prop.getProperty("urlBaycity"));
		
		HomePageBaycity hpb=new HomePageBaycity(driver);
		hpb.getLocationSwitcher().click();
		hpb.getSherman().click();
		log.info("Switching to Sherman location");
		
		HomePageSherman hps= new HomePageSherman(driver);
		
		int popup = hps.getFuncardPopup().size();
		if(popup>0) {
			hps.getCloseicon().click();
			log.info("Closing Funcard popup");
		}
		
		Actions a = new Actions(driver);
		a.moveToElement(driver.findElement(By.xpath("//*[text()='Movies']/parent::a"))).build().perform();
		hpb.getShowtimes().click();
		log.info("Navigating to Showtimes screen");
		
		
		ShowtimesPageSherman shps=new ShowtimesPageSherman(driver);
		
		Assert.assertTrue(shps.getSliderSection().isDisplayed());
		log.info("Movie Slider section is displayed");
		
		Assert.assertTrue(shps.getShowTimeSection().isDisplayed());
		log.info("Show time section is displayed.");
		
		
			
	}
	@AfterTest
	public void tearDown()
	{
		driver.quit();
	}

}
