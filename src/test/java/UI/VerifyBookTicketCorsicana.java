package UI;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import PageObjects.HomePageBaycity;
import PageObjects.HomePageCorsicana;
import PageObjects.MoviesPageBaycity;
import PageObjects.MoviesPageCorsicana;
import resources.base;

public class VerifyBookTicketCorsicana extends base {
	
	public static Logger log=LogManager.getLogger(base.class.getName());
	
	@BeforeTest
	public void initialize() throws IOException
	{
		driver = InitializeDriver();
		driver.manage().window().maximize();
	}
	@Test
	public void VerifyBookTicketCorsicana() throws InterruptedException {
		driver.get(prop.getProperty("urlBaycity"));
		
		HomePageBaycity hpb=new HomePageBaycity(driver);
		hpb.getLocationSwitcher().click();
		hpb.getCorsicana().click();
		log.info("Switching to Corsicana location");
		
		HomePageCorsicana hmc = new HomePageCorsicana(driver);
		log.info("Navigating to Movies screen");
		hmc.getMoviesTab().click();
		
		
		MoviesPageCorsicana mpc = new MoviesPageCorsicana(driver);
		
		log.info("Select a movie");
		mpc.getBookableItem().click();
	
		log.info("Click Buy tickets");
		mpc.getBuytickets().click();
		Thread.sleep(2000);
			
		log.info("Click Tomorrow tab");
		mpc.getTomorrowTab().click();
		
		log.info("Select a timeslot");
		mpc.getTimeslot().click();
		Thread.sleep(3000);
		
		
		Actions a = new Actions(driver);
		a.moveToElement(mpc.getTicketholder());
		a.click();
		a.sendKeys("1");
		a.build().perform();


		log.info("Click Next");
		mpc.getNext().click();
		
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,250)", "");
		Thread.sleep(1000);
		
		log.info("Assert cart item shows up");
		Assert.assertTrue(mpc.getCartItem().isDisplayed());
		
		log.info("Clicking the Submit button");
		mpc.getSubmit().click();
		
		log.info("Assert Checkout block is displayed ");
		Assert.assertTrue(mpc.getCheckoutBlock().isDisplayed());
		
		
	}
	@AfterTest
	public void tearDown()
	{
		driver.quit();
	}

}
