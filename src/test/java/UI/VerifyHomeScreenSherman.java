package UI;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import PageObjects.HomePageBaycity;
import PageObjects.HomePageCorsicana;
import PageObjects.HomePageSherman;
import resources.base;

public class VerifyHomeScreenSherman extends base {
	
	public static Logger log=LogManager.getLogger(base.class.getName());
	
	@BeforeTest
	public void initialize() throws IOException
	{
		driver = InitializeDriver();
		driver.manage().window().maximize();
	}
	@Test
	public void VerifyHomeScreenSherman() throws InterruptedException {
		driver.get(prop.getProperty("urlBaycity"));
		
		HomePageBaycity hpb=new HomePageBaycity(driver);
		hpb.getLocationSwitcher().click();
		hpb.getSherman().click();
		log.info("Switching to Sherman location");
		
		HomePageSherman hms = new HomePageSherman(driver);
		Assert.assertTrue(hms.getSlidersSection().isDisplayed());
		log.info("Slider section is displayed");
		
		Assert.assertTrue(hms.getPartiesAndEvents().isDisplayed());
		log.info("Parties and Events section is displayed.");
		
		Assert.assertTrue(hms.getNowIntTheTheatre().isDisplayed());
		log.info("Now in the Theatre section is displayed.");
		
		Assert.assertTrue(hms.getFuncardBlock().isDisplayed());
		log.info("Get Funcard section is displayed.");

		
	}
	@AfterTest
	public void tearDown()
	{
		driver.quit();
	}

}
