package UI;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import PageObjects.HomePageBaycity;
import PageObjects.MoviesPageBaycity;
import PageObjects.MoviesPageCorsicana;
import resources.base;

public class VerifyMoviesScreenCorsicana extends base {
	
	public static Logger log=LogManager.getLogger(base.class.getName());
	
	@BeforeTest
	public void initialize() throws IOException
	{
		driver = InitializeDriver();
		driver.manage().window().maximize();
	}
	@Test
	public void VerifyMoviesScreenCorsicana() throws InterruptedException {
		driver.get(prop.getProperty("urlBaycity"));
		
		HomePageBaycity hpb=new HomePageBaycity(driver);
		hpb.getLocationSwitcher().click();
		hpb.getCorsicana().click();
		log.info("Switching to Corsicana location");
		
		hpb.getMoviesTab().click();
		log.info("Navigating to Movies screen");
		
		
		MoviesPageCorsicana mpc = new MoviesPageCorsicana(driver);
		
		Assert.assertTrue(mpc.getSliderSection().isDisplayed());
		log.info("Movie Slider section is displayed");
		
		Assert.assertTrue(mpc.getNowInTheTheatre().isDisplayed());
		log.info("Now in the Theatre section is displayed.");
		
		Assert.assertTrue(mpc.getMoviesComingSoon().isDisplayed());
		log.info("Movies Coming Soon block is displayed.");
		
		
			
	}
	@AfterTest
	public void tearDown()
	{
		driver.quit();
	}

}
