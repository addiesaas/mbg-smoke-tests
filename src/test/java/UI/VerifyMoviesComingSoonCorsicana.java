package UI;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import PageObjects.ComingSoonPageBaycity;
import PageObjects.ComingSoonPageCorsicana;
import PageObjects.HomePageBaycity;
import PageObjects.MoviesPageBaycity;
import PageObjects.ShowtimesPageBaycity;
import resources.base;

public class VerifyMoviesComingSoonCorsicana extends base {
	
	public static Logger log=LogManager.getLogger(base.class.getName());
	
	@BeforeTest
	public void initialize() throws IOException
	{
		driver = InitializeDriver();
		driver.manage().window().maximize();
	}
	
	@Test
	public void VerifyMoviesComingSoonCorsicana() throws InterruptedException {
		driver.get(prop.getProperty("urlBaycity"));
		
		
		HomePageBaycity hpb=new HomePageBaycity(driver);
		hpb.getLocationSwitcher().click();
		hpb.getCorsicana().click();
		log.info("Switching to Corsicana location");
		
		Actions a = new Actions(driver);
		a.moveToElement(driver.findElement(By.xpath("//*[text()='Movies']/parent::a"))).build().perform();
		hpb.getComingSoon().click();
		log.info("Navigating to Coming Soon screen");
		
		
		ComingSoonPageCorsicana cspc=new ComingSoonPageCorsicana(driver);
		
		Assert.assertTrue(cspc.getSliderSection().isDisplayed());
		log.info("Movie Slider section is displayed");
		
		Assert.assertTrue(cspc.getMoviesComingSoon().isDisplayed());
		log.info("Show time section is displayed.");
		
		
			
	}
	@AfterTest
	public void tearDown()
	{
		driver.quit();
	}

}
