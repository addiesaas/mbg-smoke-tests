package UI;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import PageObjects.HomePageBaycity;
import PageObjects.MoviesPageBaycity;
import resources.base;

public class VerifyBookTicketBaycity extends base {
	
	public static Logger log=LogManager.getLogger(base.class.getName());
	
	@BeforeTest
	public void initialize() throws IOException
	{
		driver = InitializeDriver();
		driver.manage().window().maximize();
	}
	@Test
	public void VerifyBookTicketBaycity() throws InterruptedException {
		driver.get(prop.getProperty("urlBaycity"));
		
		HomePageBaycity hpb=new HomePageBaycity(driver);
		log.info("Navigating to Movies screen");
		hpb.getMoviesTab().click();
		
		
		MoviesPageBaycity mpb = new MoviesPageBaycity(driver);
		log.info("Select a bookable item");
		//iterate through available movies
		for(int i = 1;i <= 4;i++) {
			driver.findElement(By.xpath("(//a[text()='More showtimes'])["+i+"]")).click();
			
			log.info("Click Buy tickets");
			mpb.getBuytickets().click();
			Thread.sleep(2000);
			
			log.info("Click Tomorrow tab");
			mpb.getTomorrowTab().click();
			
			int count = mpb.getTab().size();
			System.out.println(count);
			if(count == 0) {
				driver.navigate().back();
			}
			else if(count == 1) {
				break;
			}
			
			
		}
		
		log.info("Select a timeslot 11:00 am");
		mpb.getTimeslot().click();
		
		log.info("Select a seat");
		mpb.getSeat().click();
		
		
		log.info("Click Add to cart button");
		mpb.getAddToCart().click();
		
		log.info("Assert timer alert shows up");
		Assert.assertTrue(mpb.getTimerAlert().isDisplayed());
		
		log.info("Assert cart item shows up");
		Assert.assertTrue(mpb.getCartItem().isDisplayed());
		
		log.info("Clicking the Submit button");
		mpb.getSubmit().click();
		
		log.info("Assert Checkout block is displayed ");
		Assert.assertTrue(mpb.getCheckoutBlock().isDisplayed());
		
		
	}
	@AfterTest
	public void tearDown()
	{
		driver.quit();
	}

}
