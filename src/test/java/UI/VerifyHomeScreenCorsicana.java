package UI;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import PageObjects.HomePageBaycity;
import PageObjects.HomePageCorsicana;
import resources.base;

public class VerifyHomeScreenCorsicana extends base {
	
	public static Logger log=LogManager.getLogger(base.class.getName());
	
	@BeforeTest
	public void initialize() throws IOException
	{
		driver = InitializeDriver();
		driver.manage().window().maximize();
	}
	@Test
	public void VerifyHomeScreenCorsicana() throws InterruptedException {
		driver.get(prop.getProperty("urlBaycity"));
		
		HomePageBaycity hpb=new HomePageBaycity(driver);
		hpb.getLocationSwitcher().click();
		hpb.getCorsicana().click();
		log.info("Switching to Corsicana location");
		
		HomePageCorsicana hmc = new HomePageCorsicana(driver);
		Assert.assertTrue(hmc.getSlidersSection().isDisplayed());
		log.info("Slider section is displayed");
		
		Assert.assertTrue(hmc.getPartiesAndEvents().isDisplayed());
		log.info("Parties and Events section is displayed.");
		
		Assert.assertTrue(hmc.getNowIntTheTheatre().isDisplayed());
		log.info("Now in the Theatre section is displayed.");
		
		Assert.assertTrue(hmc.getFuncardBlock().isDisplayed());
		log.info("Get Funcard section is displayed.");
		
		Assert.assertTrue(hmc.getSocialMediaBlock().isDisplayed());
		log.info("Social Media block is displayed.");
		
	}
	@AfterTest
	public void tearDown()
	{
		driver.quit();
	}

}
