package UI;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import PageObjects.HomePageBaycity;
import PageObjects.HomePageSherman;
import PageObjects.MoviesPageBaycity;
import PageObjects.MoviesPageCorsicana;
import PageObjects.MoviesPageSherman;
import resources.base;

public class VerifyMoviesScreenSherman extends base {
	
	public static Logger log=LogManager.getLogger(base.class.getName());
	
	@BeforeTest
	public void initialize() throws IOException
	{
		driver = InitializeDriver();
		driver.manage().window().maximize();
	}
	@Test
	public void VerifyMoviesScreenSherman() throws InterruptedException {
		driver.get(prop.getProperty("urlBaycity"));
		
		HomePageBaycity hpb=new HomePageBaycity(driver);
		hpb.getLocationSwitcher().click();
		hpb.getSherman().click();
		log.info("Switching to Sherman location");
		
		
		HomePageSherman hps= new HomePageSherman(driver);
		
		int popup = hps.getFuncardPopup().size();
		if(popup>0) {
			log.info("Closing Funcard popup");
			hps.getCloseicon().click();
			
		}
		
		log.info("Navigating to Movies screen");
		hps.getMoviesTab().click();
		
		
		
		MoviesPageSherman mps = new MoviesPageSherman(driver);
		
		
		Assert.assertTrue(mps.getSliderSection().isDisplayed());
		log.info("Movie Slider section is displayed");
		
		Assert.assertTrue(mps.getNowInTheTheatre().isDisplayed());
		log.info("Now in the Theatre section is displayed.");
		
		Assert.assertTrue(mps.getMoviesComingSoon().isDisplayed());
		log.info("Movies Coming Soon block is displayed.");
		
		
			
	}
	@AfterTest
	public void tearDown()
	{
		driver.quit();
	}

}
