package UI;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import PageObjects.ComingSoonPageBaycity;
import PageObjects.HomePageBaycity;
import PageObjects.MoviesPageBaycity;
import PageObjects.ShowtimesPageBaycity;
import resources.base;

public class VerifyMoviesComingSoonBaycity extends base {
	
	public static Logger log=LogManager.getLogger(base.class.getName());
	
	@BeforeTest
	public void initialize() throws IOException
	{
		driver = InitializeDriver();
		driver.manage().window().maximize();
	}
	
	@Test
	public void VerifyMoviesComingSoonBaycity() throws InterruptedException {
		driver.get(prop.getProperty("urlBaycity"));
		
		HomePageBaycity hpb=new HomePageBaycity(driver);
		
		Actions a = new Actions(driver);
		a.moveToElement(driver.findElement(By.xpath("//*[text()='Movies']/parent::a"))).build().perform();
		hpb.getComingSoon().click();
		log.info("Navigating to Coming Soon screen");
		
		
		ComingSoonPageBaycity cspb=new ComingSoonPageBaycity(driver);
		
		Assert.assertTrue(cspb.getSliderSection().isDisplayed());
		log.info("Movie Slider section is displayed");
		
		Assert.assertTrue(cspb.getShowTimeSection().isDisplayed());
		log.info("Show time section is displayed.");
		
		
			
	}
	@AfterTest
	public void tearDown()
	{
		driver.quit();
	}

}
