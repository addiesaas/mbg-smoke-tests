package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePageBaycity {
	
public static WebDriver driver;
	
	public HomePageBaycity(WebDriver driver)
	{
		this.driver=driver;
	}
	By signIn = By.cssSelector("div[class='addiesaas-switch-guest']");
	By slidersSection = By.xpath("//div[@class='owl-stage']//div[@class='owl-item active']");
	By partiesAndEvents = By.xpath("//div[@class='elementor-row']/div[starts-with(@class, 'elementor-element elementor-element-bed')]");
	By nowInTheTheatre = By.xpath("//div[@class='movie-booking-movies-list-mini']//div[@class='movie-card']");
	By funcardsBlock = By.xpath("//div[@data-id='10c3598c']//section");
	By socialMediaBlock = By.cssSelector("div[class='slick-list draggable']");
	By moviesTab = By.xpath("//*[text()='Movies']/parent::a");
	By showTimes = By.xpath("(//*[text()='Showtimes'])[1]");
	By comingSoon = By.xpath("(//*[text()='Coming Soon'])[1]");
	By locationSwitcher = By.xpath("(//ul[@class='location-list location-list-js'])[2]");
	By corsicana = By.xpath("//ul[@class='location-list location-list-js opened']/li[2]/a[@class='location-name']");
	By sherman = By.xpath("//ul[@class='location-list location-list-js opened']/li[3]/a[@class='location-name']");
	By partiesEvents = By.xpath("//span[text() = 'Parties & Events']");
	
	public WebElement getSignIn() {
		return driver.findElement(signIn);
	}
	public WebElement getSlidersSection() {
		return driver.findElement(slidersSection);
	}
	public WebElement getPartiesAndEvents() {
		return driver.findElement(partiesAndEvents);
	}
	public WebElement getNowIntTheTheatre() {
		return driver.findElement(nowInTheTheatre);
	}
	public WebElement getFuncardBlock() {
		return driver.findElement(funcardsBlock);
	}
	public WebElement getSocialMediaBlock() {
		return driver.findElement(socialMediaBlock);
	}
	public WebElement getMoviesTab() {
		return driver.findElement(moviesTab);
	}
	public WebElement getShowtimes() {
		return driver.findElement(showTimes);
	}
	public WebElement getComingSoon() {
		return driver.findElement(comingSoon);
	}
	public WebElement getLocationSwitcher() {
		return driver.findElement(locationSwitcher);
	}
	public WebElement getCorsicana() {
		return driver.findElement(corsicana);
	}
	public WebElement getSherman() {
		return driver.findElement(sherman);
	}
	public WebElement getPartiesEvents() {
		return driver.findElement(partiesEvents);
	}
}
