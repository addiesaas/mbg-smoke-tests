package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MoviesPageCorsicana {
	
public static WebDriver driver;
	
	public MoviesPageCorsicana(WebDriver driver)
	{
		this.driver=driver;
	}
	By sliderSection = By.xpath("//section[@data-id='ab63993']//div[@class='owl-item active']");
	By nowInTheTheatre = By.xpath("//div[@data-id='083d7cd']//div[@class='movie-card']");
	By moviesComingSoon = By.xpath("//div[@data-id='5510bc2']//div[@class='movie-card']");
	By bookableItem = By.xpath("(//a[text()='More showtimes'])[1]");
	By buyTickets = By.xpath("//button[@class='btn btn-accent hoverEffect']");
	By tomorrowTab = By.xpath("//ul[@class='selector-container horizontal noRadio noCssRadio']/li[2]");
	By timeSlot = By.xpath("//div[@class='film-session-card__session']/a[1]");
	By ticketHolder = By.xpath("(//div[@class='form-field m-b-0'])[1]");
	By next = By.xpath("//button[@type='button']");
	By cartItem = By.xpath("//div[@class='movie-item']");
	By submit = By.xpath("//button[@class='btn btn-next btn-accent']");
	By checkoutBlock = By.xpath("//form[@class='form--checkout']//div[@class='form-col']");
	
	
	
	
	public WebElement getSliderSection() {
		return driver.findElement(sliderSection);
	}
	public WebElement getNowInTheTheatre() {
		return driver.findElement(nowInTheTheatre);
	}
	public WebElement getMoviesComingSoon() {
		return driver.findElement(moviesComingSoon);
	}
	public WebElement getBookableItem() {
		return driver.findElement(bookableItem);
	}
	public WebElement getBuytickets() {
		return driver.findElement(buyTickets);
	}
	public WebElement getTomorrowTab() {
		return driver.findElement(tomorrowTab);
	}
	public WebElement getTimeslot() {
		return driver.findElement(timeSlot);
	}
	public WebElement getTicketholder() {
		return driver.findElement(ticketHolder);
	}
	public WebElement getNext() {
		return driver.findElement(next);
	}
	public WebElement getCartItem() {
		return driver.findElement(cartItem);
	}
	public WebElement getSubmit() {
		return driver.findElement(submit);
	}
	public WebElement getCheckoutBlock() {
		return driver.findElement(checkoutBlock);
	}
}
