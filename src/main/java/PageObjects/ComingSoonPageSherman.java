package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ComingSoonPageSherman {
	
public static WebDriver driver;
	
	public ComingSoonPageSherman(WebDriver driver)
	{
		this.driver=driver;
	}
	By sliderSection = By.xpath("//section[@data-id='b73c94e']//div[@class='owl-item active']");
	By moviesComingSoon = By.xpath("//div[@data-id='a6d46fe']//div[@class='movie-card']");
	
	
	
	
	
	
	
	public WebElement getSliderSection() {
		return driver.findElement(sliderSection);
	}
	public WebElement getMoviesComingSoon() {
		return driver.findElement(moviesComingSoon);
	}

}
