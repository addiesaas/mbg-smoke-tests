package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ShowtimesPageSherman {
	
public static WebDriver driver;
	
	public ShowtimesPageSherman(WebDriver driver)
	{
		this.driver=driver;
	}
	By sliderSection = By.xpath("//section[@data-id='27c880d']//div[@class='owl-item active']");
	By showTimesSection = By.xpath("//div[@class='show-movie-list']/div[@class='show-movie-list-item']");
	
	
	
	
	
	
	
	public WebElement getSliderSection() {
		return driver.findElement(sliderSection);
	}
	public WebElement getShowTimeSection() {
		return driver.findElement(showTimesSection);
	}

}
