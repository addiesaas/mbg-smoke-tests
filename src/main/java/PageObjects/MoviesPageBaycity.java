package PageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MoviesPageBaycity {
	
public static WebDriver driver;
	
	public MoviesPageBaycity(WebDriver driver)
	{
		this.driver=driver;
	}
	By sliderSection = By.xpath("//section[@data-id='ab63993']//div[@class='owl-item active']");
	By nowInTheTheatre = By.xpath("//div[@data-id='7e5a41c']//div[@class='movie-card']");
	By moviesComingSoon = By.xpath("//div[@data-id='718236c']//div[@class='movie-card']");
	By bookableItem = By.xpath("(//a[text()='More showtimes'])[3]");
	By tomorrowTab = By.xpath("//ul[@class='selector-container horizontal noRadio noCssRadio']/li[2]");
	By buyTickets = By.xpath("//button[@class='btn btn-accent hoverEffect']");
	By timeSlot = By.xpath("//a[text()='11:00 am']");
	By seat = By.xpath("//div[@class='cinema-hall-seats']/div[@data-type='16'][@data-col='1']");
	By addToCart = By.xpath("//a[@class='btn btn-accent']");
	By timerAlert = By.xpath("(//div[@class='re-toast-alert-content'])[1]");
	By cartItem = By.xpath("//div[@class='movie-cart-item__content movie-cart-item']");
	By submit = By.xpath("//button[@class='btn btn-next btn-accent']");
	By checkoutBlock = By.xpath("//form[@class='form--checkout']//div[@class='form-col']");
	
	
	
	
	public WebElement getSliderSection() {
		return driver.findElement(sliderSection);
	}
	public WebElement getNowInTheTheatre() {
		return driver.findElement(nowInTheTheatre);
	}
	public WebElement getMoviesComingSoon() {
		return driver.findElement(moviesComingSoon);
	}
	public WebElement getBookableitem() {
		return driver.findElement(bookableItem);
	}
	public WebElement getTomorrowTab() {
		return driver.findElement(tomorrowTab);
	}
	public WebElement getBuytickets() {
		return driver.findElement(buyTickets);
	}
	public WebElement getTimeslot() {
		return driver.findElement(timeSlot);
	}
	public WebElement getSeat() {
		return driver.findElement(seat);
	}
	public WebElement getAddToCart() {
		return driver.findElement(addToCart);
	}
	public WebElement getTimerAlert() {
		return driver.findElement(timerAlert);
	}
	public WebElement getCartItem() {
		return driver.findElement(cartItem);
	}
	public WebElement getSubmit() {
		return driver.findElement(submit);
	}
	public WebElement getCheckoutBlock() {
		return driver.findElement(checkoutBlock);
	}
	public List<WebElement> getTab() {
		return driver.findElements(timeSlot);
	}
}
