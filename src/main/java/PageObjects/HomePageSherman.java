package PageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePageSherman {
	
public static WebDriver driver;
	
	public HomePageSherman(WebDriver driver)
	{
		this.driver=driver;
	}
	By slidersSection = By.xpath("//div[@class='owl-stage']//div[@class='owl-item active']");
	By partiesAndEvents = By.xpath("//div[@class='elementor-row']/div[starts-with(@class, 'elementor-element elementor-element-bed')]");
	By nowInTheTheatre = By.xpath("//div[@class='movie-booking-movies-list-mini']//div[@class='movie-card']");
	By funcardsBlock = By.xpath("//div[@data-id='10c3598c']//section");
	By moviesTab = By.xpath("//*[text()='Movies']/parent::a");
	By funcardPopup = By.xpath("//div[@class='mbg-popup clearfix']");
	By closeIcon = By.cssSelector("div.mbg-popup.clearfix a.mbg-close");
	



	public WebElement getSlidersSection() {
		return driver.findElement(slidersSection);
	}
	public WebElement getPartiesAndEvents() {
		return driver.findElement(partiesAndEvents);
	}
	public WebElement getNowIntTheTheatre() {
		return driver.findElement(nowInTheTheatre);
	}
	public WebElement getFuncardBlock() {
		return driver.findElement(funcardsBlock);
	}
	public WebElement getMoviesTab() {
		return driver.findElement(moviesTab);
	}
	public List<WebElement> getFuncardPopup() {
		return driver.findElements(funcardPopup);
	}
	public WebElement getCloseicon() {
		return driver.findElement(closeIcon);
	}

}
