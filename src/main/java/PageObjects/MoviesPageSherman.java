package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MoviesPageSherman {
	
public static WebDriver driver;
	
	public MoviesPageSherman(WebDriver driver)
	{
		this.driver=driver;
	}
	By sliderSection = By.xpath("//section[@data-id='ab63993']//div[@class='owl-item active']");
	By nowInTheTheatre = By.xpath("//div[@data-id='122c891']//div[@class='movie-card']");
	By moviesComingSoon = By.xpath("//div[@data-id='c397fc4']//div[@class='movie-card']");
	
	
	
	
	
	public WebElement getSliderSection() {
		return driver.findElement(sliderSection);
	}
	public WebElement getNowInTheTheatre() {
		return driver.findElement(nowInTheTheatre);
	}
	public WebElement getMoviesComingSoon() {
		return driver.findElement(moviesComingSoon);
	}
	
}
