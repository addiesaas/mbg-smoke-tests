package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePageCorsicana {
	
public static WebDriver driver;
	
	public HomePageCorsicana(WebDriver driver)
	{
		this.driver=driver;
	}
	By slidersSection = By.xpath("//div[@class='owl-stage']//div[@class='owl-item active']");
	By partiesAndEvents = By.xpath("//div[@class='elementor-row']/div[starts-with(@class, 'elementor-element elementor-element-bed')]");
	By nowInTheTheatre = By.xpath("//div[@class='movie-booking-movies-list-mini']//div[@class='movie-card']");
	By funcardsBlock = By.xpath("//div[@data-id='7cbfcfef']//section");
	By socialMediaBlock = By.cssSelector("div[class='slick-list draggable']");
	By moviesTab = By.xpath("//*[text()='Movies']/parent::a");
	By showTimes = By.xpath("(//*[text()='Showtimes'])[1]");
	By comingSoon = By.xpath("(//*[text()='Coming Soon'])[1]");


	public WebElement getSlidersSection() {
		return driver.findElement(slidersSection);
	}
	public WebElement getPartiesAndEvents() {
		return driver.findElement(partiesAndEvents);
	}
	public WebElement getNowIntTheTheatre() {
		return driver.findElement(nowInTheTheatre);
	}
	public WebElement getFuncardBlock() {
		return driver.findElement(funcardsBlock);
	}
	public WebElement getSocialMediaBlock() {
		return driver.findElement(socialMediaBlock);
	}
	public WebElement getMoviesTab() {
		return driver.findElement(moviesTab);
	}
	public WebElement getShowtimes() {
		return driver.findElement(showTimes);
	}
	public WebElement getComingSoon() {
		return driver.findElement(comingSoon);
	}

}
