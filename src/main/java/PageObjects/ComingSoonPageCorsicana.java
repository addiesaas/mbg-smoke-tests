package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ComingSoonPageCorsicana {
	
public static WebDriver driver;
	
	public ComingSoonPageCorsicana(WebDriver driver)
	{
		this.driver=driver;
	}
	By sliderSection = By.xpath("//section[@data-id='b73c94e']//div[@class='owl-item active']");
	By moviesComingSoon = By.xpath("//div[@data-id='704c32d']//div[@class='movie-card']");
	
	
	
	
	
	
	
	public WebElement getSliderSection() {
		return driver.findElement(sliderSection);
	}
	public WebElement getMoviesComingSoon() {
		return driver.findElement(moviesComingSoon);
	}

}
